import time
import random
from merge_sort import mergesort


seq = random.sample(range(10000), 10000)
start = time.time()
mergesort(seq)
print("{:.3f}".format(time.time() - start))
