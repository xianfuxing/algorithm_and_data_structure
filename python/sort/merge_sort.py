import time
import random


def mergesort(seq):
    if len(seq) <= 1:
        return seq
    mid = len(seq) // 2
    left = mergesort(seq[:mid])
    right = mergesort(seq[mid:])

    return merge(left, right)


def merge(left, right):
    i = 0
    j = 0
    result = []
    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    result += left[i:]
    result += right[j:]
    return result


if __name__ == '__main__':
    seq = [5, 3, 0, 6, 1, 4, 2]
    ret = mergesort(seq)
    print(ret)
