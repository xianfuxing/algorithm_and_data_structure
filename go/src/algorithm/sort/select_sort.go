// 每一轮选择最小的元素放在第一位，直到排序完毕。
// 1. 设置minIndex，每比较一次就确认 minIndex 是否需要替换。
// 2. 外层循环一次后，就教本次最小的值赋给a[i] 也就是 a[i], a[minIndex] 互换。

package sort

func SelectSort(arr []int) {
	var minIndex int
	n := len(arr)
	//count := 0
	for i := 0; i < n; i++ {
		// 寻找[i, n)区间里最小的值
		minIndex = i
		for j := i+1; j < n; j++ {
			if arr[j] < arr[minIndex] {
				minIndex = j
			}
		}
		arr[i], arr[minIndex] = arr[minIndex], arr[i]
		//count++
	}
	//fmt.Printf("switch %d\n", count)
}