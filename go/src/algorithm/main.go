package main

import (
	_heap "algorithm/heap"
	_sort "algorithm/sort"
	"fmt"
)

func testSort() {
	n := 1000000
	swapTimes := 100
	seq1 := _sort.GenerateRandomArray(n, 0, n)
	seq2 := make([]int, len(seq1))
	seq11:= make([]int, len(seq1))
	seq12:= make([]int, len(seq1))
	seq13:= make([]int, len(seq1))
	copy(seq2, seq1)
	copy(seq11, seq1)
	copy(seq12, seq1)
	copy(seq13, seq1)

	seq3 := _sort.GenerateNearlyOrderedArray(n, swapTimes)
	seq4 := make([]int, len(seq3))
	seq21 := make([]int, len(seq3))
	seq22 := make([]int, len(seq3))
	seq23 := make([]int, len(seq3))
	copy(seq4, seq3)
	copy(seq12, seq4)
	copy(seq22, seq4)
	copy(seq23, seq4)

	seq5 := _sort.GenerateRandomArray(n, 0, 10)
	seq6 := make([]int, len(seq5))
	seq31 := make([]int, len(seq5))
	seq32 := make([]int, len(seq5))
	seq33 := make([]int, len(seq5))
	copy(seq6, seq5)
	copy(seq31, seq5)
	copy(seq32, seq5)
	copy(seq33, seq5)

	fmt.Printf("Test for random array, size = %d, random range [0, %d]\n", n, n)
	_sort.TestSort("MergeSort2", _sort.MergeSort2, seq1)
	_sort.TestSort("QuickSort2", _sort.QuickSort2, seq2)
	_sort.TestSort("HeapSort1", _heap.HeapSort, seq11)
	_sort.TestSort("HeapSort2", _heap.HeapSort2, seq12)
	_sort.TestSort("HeapSort3", _heap.HeapSort3, seq13)

	fmt.Printf("Test for random array, size = %d, swap times %d\n", n, swapTimes)
	_sort.TestSort("MergeSort2", _sort.MergeSort2, seq3)
	_sort.TestSort("QuickSort2", _sort.QuickSort2, seq4)
	_sort.TestSort("HeapSort1", _heap.HeapSort, seq21)
	_sort.TestSort("HeapSort2", _heap.HeapSort2, seq22)
	_sort.TestSort("HeapSort3", _heap.HeapSort3, seq23)

	fmt.Printf("Test for random array, size = %d, random range [0,10]\n", n)
	_sort.TestSort("MergeSort2", _sort.MergeSort2, seq5)
	_sort.TestSort("QuickSort2", _sort.QuickSort2, seq6)
	_sort.TestSort("HeapSort1", _heap.HeapSort, seq31)
	_sort.TestSort("HeapSort2", _heap.HeapSort2, seq32)
	_sort.TestSort("HeapSort3", _heap.HeapSort3, seq33)

}

func main() {
	testSort()
}